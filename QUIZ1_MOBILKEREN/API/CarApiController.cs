﻿using QUIZ1_MOBILKEREN.Models;
using QUIZ1_MOBILKEREN.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QUIZ1_MOBILKEREN.API
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeApiController : ControllerBase
    {
        private readonly EmployeeService ServiceMan;

        public EmployeeApiController(EmployeeService employeeService)
        {
            this.ServiceMan = employeeService;
        }

        [HttpGet("all-employee", Name = "allEmployee")]
        public async Task<IActionResult> GetAllEmployeesAsync()
        {
            var employees = await ServiceMan.GetAllEmployeesAsync();
            return Ok(employees);
        }

        [HttpPost("insert", Name = "insertEmployee")]
        public async Task<ActionResult<ResponseModel>> InsertNewEmployeeAsync([FromBody]EmployeeModel value)
        {
            var isSuccess = await ServiceMan.InsertEmployee(value);
            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert new data."
            });
        }

        [HttpPut("update", Name = "updateEmployee")]
        public async Task<ActionResult<ResponseModel>> UpdateEmployeeAsync([FromBody] EmployeeModel value)
        {
            var isSuccess = await ServiceMan.UpdateEmployeeAsync(value);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success update data {value.Name}"
            });
        }

        [HttpDelete("delete", Name = "deleteEmployee")]
        public async Task<ActionResult<ResponseModel>> DeleteEmployeeAsync([FromBody] EmployeeModel value)
        {
            var isSuccess = await ServiceMan.DeleteEmployeeAsync(value.EmployeeId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success delete employee {value.Name}"
            });
        }

        [HttpGet("specific-employee", Name = "getSpesificEmployee")]
        public async Task<ActionResult<EmployeeModel>> GetSpecificEmployeeAsync(Guid? employeeId)
        {
            if (employeeId.HasValue == false)
            {
                return BadRequest(null);
            }

            var employee = await ServiceMan.GetSpesificEmployeeAsync(employeeId.Value);
            if (employee == null)
            {
                return BadRequest(null);
            }
            return Ok(employee);
        }

        [HttpDelete("delete-2/{employeeId}", Name = "deleteEmployee2")] //API With Parameter
        public async Task<ActionResult<ResponseModel>> DeleteEmployee2Async(Guid employeeId)
        {
            var isSuccess = await ServiceMan.DeleteEmployeeAsync(employeeId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Employee Id not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success delete employee"
            });
        }

        [HttpDelete("delete-query")]
        public async Task<IActionResult> DeleteEmployeeQueryAsync([FromQuery] Guid employeeId)
        {
            var isSuccess = await ServiceMan.DeleteEmployeeAsync(employeeId);

            if (isSuccess == false)
            {
                return BadRequest("Id not found!");
            }

            return Ok(isSuccess);
        }


        [HttpGet("filter-data")]
        public async Task<ActionResult<List<EmployeeModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await ServiceMan.GetAsync(pageIndex, itemPerPage, filterByName);

            return Ok(data);
        }

        [HttpGet("total-data")]
        public ActionResult<int> GetTotalData()
        {
            var totalData = ServiceMan.GetTotalData();

            return Ok(totalData);
        }
    }

}
