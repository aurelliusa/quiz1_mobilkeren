﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using QUIZ1_MOBILKEREN.Models;
using QUIZ1_MOBILKEREN.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace QUIZ1_MOBILKEREN.API
{
    [Route("api/v1/mobilkeren-app")]
    [ApiController]
    public class EmployeeAppApiController : ControllerBase
    {
        private readonly EmployeeAppService _appServiceMan;

        public EmployeeAppApiController(EmployeeAppService employeeAppService)
        {
            this._appServiceMan = employeeAppService;
        }

        [HttpGet("specific-employee-app", Name = "GetSpecificEmployeeApp")]
        public async Task<ActionResult<List<EmployeeModelApp>>> GetAllDataAsync(Guid? employeeId)
        {
            if (employeeId.HasValue == false)
            {
                return BadRequest(null);
            }

            var employee = await _appServiceMan.GetAsync(employeeId);

            if (employee == null)
            {
                return BadRequest(null);
            }
            return Ok(employee);
        }

        [HttpGet("specific-app", Name = "GetSpecificApp")]
        public async Task<ActionResult<EmployeeModelApp>> GetSpecificApp(string appId)
        {
            if (string.IsNullOrEmpty(appId) == true)
            {
                return BadRequest(null);
            }

            var id = Int32.Parse(appId);
            var app = await _appServiceMan.GetSpecificAppAsync(id);

            if (app == null)
            {
                return BadRequest(null);
            }
            return Ok(app);
        }

        [HttpPost("insert-employee-app", Name = "insertEmployeeApp")]
        public async Task<ActionResult<ResponseModel>> InsertNewEmployeeAppAsync([FromBody]EmployeeModelApp value)
        {
            var isSuccess = await _appServiceMan.InsertNewEmployeeApp(value);

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success to insert new data."
            });
        }

        [HttpDelete("delete-app", Name = "deleteEmployeeApp")]
        public async Task<ActionResult<ResponseModel>> DeleteEmployeeAppAsync([FromBody] EmployeeAppModel value)
        {
            var isSuccess = await _appServiceMan.DeleteApp(value.EmployeeAppId);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success delete employee {value.EmployeeAppId}"
            });
        }

        [HttpPut("update-app", Name = "updateEmployeeApp")]
        public async Task<ActionResult<ResponseModel>> UpdateEmployeeAsync([FromBody] EmployeeModelApp value)
        {
            var isSuccess = await _appServiceMan.UpdateAppAsync(value);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "ID Not Found!!!"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = $"Success update data {value.Name}"
            });
        }

        [HttpGet("all-data2")]
        public async Task<IActionResult> GetAllDataAsync2()
        {
            var data = await _appServiceMan.GetAllAsync2();

            return Ok(data);
        }
    }

}
