﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QUIZ1_MOBILKEREN.Models
{
    public class MessageResponseModel
    {
        public string ResponseMessage { set; get; }
        public string Status { set; get; }
    }
}
