﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QUIZ1_MOBILKEREN.Models
{
    public class EmployeeAppModel
    {
        public string Name { get; set; }
        public int CarId { get; set; }
        public int CarAmount { get; set; }
        public int CarAppId { get; set; }
    }
}
