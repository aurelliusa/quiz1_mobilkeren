﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace QUIZ1_MOBILKEREN.Models
{
    public class CarModel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int carId { set; get; }

        [Required]
        [StringLength(7, MinimumLength = 3)]
        [Display(Name = "Nama")]
        public string Name { set; get; }

        [Required]
        [Display(Name = "Amount")]
        public int carAmount { set; get; }
    }
}
